/**************************/
Quine–McCluskey Minimiztion
/*************************/

Specifications
--------------

+ Maximum no of variables in a minterm = 10
+ Range of minterms = 0 to 2^10
+ Range of value of each minterm = 0 to 2^10
+ Data structure used : Linked list
+ Doesn't deal with cyclic problems
+ Doesn't deal with Don't care
+ unlike standard representation of binary which goes from left to right, one use goes from right to left.
  For ex: 7 in represented as 1110 and not 0111
+ 2 in final solution represent "-" symbol used in real algo

=====================================================================
INSTRUCTION TO RUN THE FILE
=====================================================================

Windows
  
  - Right click on quen.c file 
  - select "open with"
  - select any complier installed in your system
  - compile and run the program 


Linux
  
  - Open terminal
  - use "cd PATH_NAME" to go in to the directory of extracted file (For ex: cd Downloads )
  - compile the file quen.c using command "gcc quen.c -lm" 
  - run the generated output file with command "./a.out"


For further assistance contact aditya.prakash@gmail.com

=======================================================================
EXPLAINATION OF CODE
=======================================================================

The programme use "Brute Force Method" to tackle the problem. So it simply follows the defintion
of the Quine–McCluskey Minimiztion algorithm to reach to solution. Hence program may be a bit slower
than what you might assume.

Making of pairs of minterms
---------------------------

All the minterms are stored in a structure along with its binary in an array. Binary of minterm is 
calculated by the function "int *to_binary(int num)". A think which to be noted is that unlike 
standard representation of binary which goes from left to right, one use goes from right to left.
For ex: 7 in represented as 1110 and not 0111

These structure of minterms are furture grouped with fucncion 
"void make_group(struct minterm_list *variable, struct minterm_list *link, int num, int FIRST)"
as per number of bits on in binary. Each group is compared with group immediately next to it and 
result is stored in a new linked list. This process is repeated until no more one bit differences are found.

Making of table and selecting prime implicants for cover
--------------------------------------------------------
Using the given list of pairs of minterms generated a 2d array implementation form 1d dynamic array, table similar 
to one use in real algo is generated where 1's mark the postion of minterms. essential prime implicants are found,
row and columns reduction is applied and this process is repeated untill all the postions of table is not set to 0.