/* strcpy example */
// #include <stdio.h>
// #include <string.h>

// int main ()
// {
//   char str1[]="Sample string";
//   char str2[40];
//   char str3[40];
//   strcpy (str2,str1);
//   strcpy (str3,"copy successful");
//   printf ("str1: %s\nstr2: %s\nstr3: %s\n",str1,str2,str3);
//   return 0;
// }

// #include <string.h>
// #include <iostream>
// using namespace std;

// main()
// {
// char name[4];
// strcpy(name, "Sam");
// cout << name;
// return (0);
// }

// #include <iostream>
// using namespace std;
// char line[100];
// int value;
// // input line from console
// // a value to double
// main ()
// {
// cout << "Enter a value: ";
// cin >> value;
// cout << "Twice " << value << " is " << value * 2 << '\n';
// return (0);
// }

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

main()
{ int no_minterms, i;
  srand(88);
  no_minterms = rand() % ((int) pow(2.0, 10.0));
  int *minterms = malloc(no_minterms*sizeof(int));
  for(i = 0; i<no_minterms; i++)
  {
  	minterms[i] = rand()%((int) pow(2.0, 10.0));
  printf("%d ", no_minterms);
  }
}

// struct abstract
// {
//   int x;
//   int y;
//   int radius;
// };

// struct a
// {
//   struct abstract abs;
//   //... other data ...
// };

// //OR Smarter way:
// void foo2(struct abstract *data)
// {
  
//   data->x = 2;
//   data->y = 3;
//   data->radius = 4;
// }
//Calling foo2 with:
// main(){
// // struct a *sa = (struct a *) malloc(sizeof(struct a));
// // foo2(&(sa->abs));
// // printf("%d\n", sa->abs.x );
// int *a;
// a = malloc(20);
// printf("%ld\n",sizeof(*a));
// printf("%ld\n", sizeof(a) );
// }

// c(int*a,int*b){
//     int d,e,i;
//     for(d=e=i=0;i-32;){
//         d+=*a>>i&1;e+=*b>>i++&1;
//     }
//     return d>e?-1:d<e;
// }
// o(r,s){qsort(r,s,4,c);}
// main() {
// 	int n;
//     static int a[] ={1, 2, 3, 4, 5, 6, 7, 8, 9};
//     o(a, 9);

//    for( n = 0 ; n < 5; n++ ) {
//       printf("%d ", a[n]);
//    }
// }
// void main()
// {
//  int a[]={7,6,15,16,10};
//  int b,i,n=0;
//  for(i=0;i<5;i++)
//  {  for(b=0,n=0;b<=sizeof(int);b++)
//  	  //printf("%d", a[i]&(1<<b));
//       (a[i]&(1<<b))?n++:n;   
//     a[i]=n;
//    printf("yo");
//    printf("%d\n", n);
//  }
//  for (i = 1; i < 7; i++) 
//   {   int tmp = a[i];
//       for (n = i; n >= 1 && tmp < a[n-1]; n--)
//          a[n] = a[n-1];
//       a[n] = tmp;
//   }   

//    for( n = 0 ; n < 7; n++ ) {
//       printf("%d ", a[n]);
//    } 
// }