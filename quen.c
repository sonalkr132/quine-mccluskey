/*---------------------------------------------------------------------------------------
|  Program to simulate static to solve Quine–McCluskey minimiztion techic.               |  
|  Doesn't deal with cyclic problems.                                                    |
|  Doesn't deal with don't care problems                                                 |
|  maximum no of variables = 10                                                          |
|  number of minterms = 0 to 2^10                                                        |
|  value of each minterm = 0 to 2^10                                                     |
|                                                                                        |
|- don't waste your time in understanding the code, it works! thats all you need to know |
|- since it uses linked list code is kinda long but on plus side unlike array            |
|  implementation its not likely to have error related to lack of memory                 |
|- 1 = True and 0 = False at most places (hopefully at all places)                       |
-----------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*******************************************************************
 defintion of structures used 
*******************************************************************/

struct minterm{
	int dec_term; // stores decimal value of minterm
	int *bin_term; //stores bianry of the minterm
  int choosen; //checks if its choosen in first itr
};

struct minterm_list{
	struct minterm var_minterm; //variable decleration for minterm
	struct minterm_list *link;
};

struct group_array{
  int *all_headers[10]; //headers of all the groups
};

struct updated_implicants{
  int *dec_minterms; //stores decimal value of monterms diff by one
  int num_dec; //numbers of decimal values in single node
  int *bin_minterms; //stores binary value of implicants with one bit diff position off
  int choosen; //keeps track if minterm was choosen after iterations
};

struct updated_implicants_list{
  struct updated_implicants var_updated; //stores updated_implicants
  struct updated_implicants_list *link;
};





/********************************************************************
function to convert decimal to binary. take input of a decimal and returns 
an array of binary in order 1 2 4 8 16... not ...16 8 4 2 1. 
********************************************************************/
int *to_binary(int num){

    int* ans = calloc(10, sizeof(int)); 
	  int i = 0; // just a counter fot while loop
    while(num>0) 
      { 
           ans[i]=num%2; 
           i++; 
           num=num/2;
      }

    return ans; //poniter to the array of the binary
}

/*******************************************************************
  function to intialize structure. takes a decimal digit as input
  and returns a structure of dec and binary associated with the number
********************************************************************/
struct minterm make_class(int num){

	struct minterm var;
	var.dec_term = num;
	var.bin_term = to_binary(num);
  var.choosen = 0;
	return var; 
    
}
/********************************************************************
 fucntion to create a linked list of minterms. takes array of
 minterms and its size as input and returns linked list's header of 
 minterms
********************************************************************/
 
struct minterm_list *make_class_list(int array_num[], int size){
    
    int i = 0; //just a counter for iterating array_num
    struct minterm_list *first_node, *second_node, *header;
    first_node = (struct minterm_list *) malloc(sizeof(struct minterm_list));
    header = first_node; //declaring header
    first_node->var_minterm = make_class(array_num[i]);
    first_node->link = NULL;
    i++; //going to second element
    while(i < size){
    	second_node = (struct minterm_list *) malloc(sizeof(struct minterm_list));
    	second_node->var_minterm = make_class(array_num[i]);
    	second_node->link = first_node->link;
    	first_node->link = second_node;
        first_node = first_node->link;
        i++;
    }

    return header;
} 

/************************************************************************
 fucntion to make a group of minterms of same number of ones in it. it takes 
 input of header of strcuture it will use to make group, a number which
 should be inserted in the group and a praramter decideing if first node is set(1),
 or not set(0)
**************************************************************************/

void make_group(struct minterm_list *variable, struct minterm_list *link, int num, int FIRST){ 
     
     if(FIRST == 0)
            { 
              variable->var_minterm = make_class(num);
              variable->link = NULL;
            }
     else
            {  
               variable->var_minterm = make_class(num);
               link->link = variable;
               variable->link = NULL;
            }
}

/**********************************************************************
 fucntion to group list as per their number of occurances of one. It takes 
 header of linked list of minterms as input and returns an array of pointers
 with each element of array pointing to a linked list of its group
***********************************************************************/

struct group_array *make_group_array(struct minterm_list *header){
     //yumm! this fucntion is meaty
     //declration of some variables needed to group different lis
     struct minterm_list *clone_header = header;
     int sum = 0;
     int i;
     struct group_array *all; //array of all headers 
     all = (struct group_array *) malloc(sizeof(struct group_array));
     //first node not set
     int first_sum_zero = 0; 
     int first_sum_one = 0; 
     int first_sum_two = 0; 
     int first_sum_three = 0;
     int first_sum_four = 0; 
     int first_sum_five = 0; 
     int first_sum_six = 0; 
     int first_sum_seven = 0; 
     int first_sum_eight = 0; 
     int first_sum_nine = 0;
     int first_sum_ten = 0;   
     //structers needed to set first node
     struct minterm_list *first_zero, *zero_header;
     struct minterm_list *first_one, *one_header;
     struct minterm_list *first_two, *two_header;
     struct minterm_list *first_three, *three_header;
     struct minterm_list *first_four, *four_header;
     struct minterm_list *first_five, *five_header;
     struct minterm_list *first_six, *six_header;
     struct minterm_list *first_seven, *seven_header;
     struct minterm_list *first_eight, *eight_header;
     struct minterm_list *first_nine, *nine_header;
     struct minterm_list *first_ten, *ten_header;
     while(clone_header!= NULL)
     {
       for(i=0; i<10; i++) sum = sum+clone_header->var_minterm.bin_term[i]; //summing all the bits, will give number of bits
       
       switch(sum)
       {
        case 0:

            if(first_sum_zero == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_zero = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              all->all_headers[0] = (int *) first_zero;
              make_group((struct minterm_list *)first_zero,(struct minterm_list *) link,clone_header->var_minterm.dec_term, 0);
              first_sum_zero = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_zero;
              second_zero = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_zero, first_zero, clone_header->var_minterm.dec_term, 1);
              first_zero = second_zero;
            }
            break;
        case 1:

            if(first_sum_one == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_one = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              all->all_headers[1] = (int *) first_one;
              make_group((struct minterm_list *)first_one,(struct minterm_list *) link, clone_header->var_minterm.dec_term, 0);
              first_sum_one = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_one;
              second_one = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_one, first_one,clone_header->var_minterm.dec_term, 1);
              first_one = second_one;
            }
            break;
        case 2:

            if(first_sum_two == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_two = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              all->all_headers[2] = (int *) first_two;
              make_group((struct minterm_list *)first_two,(struct minterm_list *) link,clone_header->var_minterm.dec_term, 0);
              first_sum_two = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_two;
              second_two = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_two, first_two, clone_header->var_minterm.dec_term, 1);
              first_two = second_two;
            }
            break;
        case 3:

            if(first_sum_three == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_three = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              all->all_headers[3] = (int *) first_three;
              make_group((struct minterm_list *)first_three,(struct minterm_list *) link, clone_header->var_minterm.dec_term, 0);
              first_sum_three = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_three;
              second_three = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_three, first_three,clone_header->var_minterm.dec_term, 1);
              first_three = second_three;
            }
            break;
        case 4:

            if(first_sum_four == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_four = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              all->all_headers[4] = (int *) first_four;
              make_group((struct minterm_list *)first_four,(struct minterm_list *) link,clone_header->var_minterm.dec_term, 0);
              first_sum_four = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_four;
              second_four = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_four, first_four, clone_header->var_minterm.dec_term, 1);
              first_four = second_four;
            }
            break;
        case 5:

            if(first_sum_five == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_five = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              all->all_headers[5] = (int *) first_five;
              make_group((struct minterm_list *)first_five,(struct minterm_list *) link, clone_header->var_minterm.dec_term, 0);
              first_sum_five = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_five;
              second_five = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_five, first_five,clone_header->var_minterm.dec_term, 1);
              first_five = second_five;
            }
            break;
        case 6:

            if(first_sum_six == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_six = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              //zero_header = first_zero; //declaring header
              all->all_headers[6] = (int *) first_six;
              make_group((struct minterm_list *)first_six,(struct minterm_list *) link,clone_header->var_minterm.dec_term, 0);
              first_sum_six = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_six;
              second_six = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_six, first_six, clone_header->var_minterm.dec_term, 1);
              first_six = second_six;
            }
            break;
        case 7:

            if(first_sum_seven == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_seven = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              //one_header = first_one; //declaring header
              all->all_headers[7] = (int *) first_seven;
              make_group((struct minterm_list *)first_seven,(struct minterm_list *) link, clone_header->var_minterm.dec_term, 0);
              first_sum_seven = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_seven;
              second_seven = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_seven, first_seven,clone_header->var_minterm.dec_term, 1);
              first_seven = second_seven;
            }
            break;
        case 8:

            if(first_sum_eight == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_eight = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              all->all_headers[8] = (int *) first_eight;
              make_group((struct minterm_list *)first_eight,(struct minterm_list *) link, clone_header->var_minterm.dec_term, 0);
              first_sum_eight = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_eight;
              second_eight = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_eight, first_eight,clone_header->var_minterm.dec_term, 1);
              first_eight = second_eight;
            }
            break;
        case 9:

            if(first_sum_nine == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_nine = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              //zero_header = first_zero; //declaring header
              all->all_headers[9] = (int *) first_nine;
              make_group((struct minterm_list *)first_nine,(struct minterm_list *) link,clone_header->var_minterm.dec_term, 0);
              first_sum_nine = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_nine;
              second_nine = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_nine, first_nine, clone_header->var_minterm.dec_term, 1);
              first_nine = second_nine;
            }
            break;
        case 10:

            if(first_sum_ten == 0)
            { 
              struct minterm_list *link;
              link = NULL;
              first_ten = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              //one_header = first_one; //declaring header
              all->all_headers[10] = (int *) first_ten;
              make_group((struct minterm_list *)first_ten,(struct minterm_list *) link, clone_header->var_minterm.dec_term, 0);
              first_sum_ten = 1; //first node is set
            }
            else
            {  
              struct minterm_list *second_ten;
              second_ten = (struct minterm_list *) malloc(sizeof(struct minterm_list));
              make_group(second_ten, first_ten,clone_header->var_minterm.dec_term, 1);
              first_ten = second_ten;
            }
            break;


       }
      clone_header = clone_header -> link; //next 
      sum = 0;
     }
     return all;
}

/***********************************************************************
 fucntion to check one bit difference. It takes input of two arrays and
 returns 0 or 1 for not a one bit diff pair or one bit diff pair
 respectively
 ************************************************************************/

int check_one_bit(int *array1, int *array2){
    int i, flag = 0;
    for(i=0; i<10; i++) if(array1[i]!=array2[i]) flag++;
    if(flag == 1) return 1; //one bit diff found
    else return 0;
 }

 /***********************************************************************
  function to update implicants with one bit diff. Takes input of array of 
  binary values of iplicants diff by one,
  returns updated_minterms (see structures definition for futher details)
  ***********************************************************************/
struct updated_implicants turn_bit_off(struct minterm first_min, struct minterm second_min){
     int i; 
     struct updated_implicants var;
     // var = (struct updated_implicants *) malloc(sizeof(struct updated_implicants));
     var.dec_minterms = malloc(2*sizeof(int));
     var.bin_minterms = calloc(10, sizeof(int));
     var.dec_minterms[0] = first_min.dec_term;
     var.dec_minterms[1] = second_min.dec_term;
     var.num_dec = 2;
     var.choosen = 0; 
     for(i=0; i<10; i++) 
     {   
         if(first_min.bin_term[i]==second_min.bin_term[i]) var.bin_minterms[i] = first_min.bin_term[i]; //set the bit as it
         else var.bin_minterms[i] = 2; //equivalent to - in the actual method
     }
     return var;
}

/**************************************************************************
 function to update all the groups of 1s. Takes input as header of array 
  of all the groups and returns array of headers of new and updated list
  basically its first iteration
***************************************************************************/
struct group_array *update_group_itr1(struct group_array *ones_headers){
     struct minterm_list *clone_first, *clone_second, *clone_second_copy, *last_check;
     struct updated_implicants_list *node1, *node2, *node_1, *node_2;
     struct group_array *all_itr; 
     int bit_diff = 0;
     int first_set = 0;
     int first_set_last = 0; //used to set 0 node of all group list
     int i; //just an iterator! you must know by now
     int match_found = 0; //not matched with any one for one bit diff
     all_itr = (struct group_array *) malloc(sizeof(struct group_array));
     for(i=1;i<11;i++)
     {
       struct minterm_list *first_header = (struct minterm_list *)ones_headers->all_headers[i-1];
       struct minterm_list *second_header = (struct minterm_list *)ones_headers->all_headers[i];
     
       clone_first = first_header;
       clone_second = second_header;
       while(clone_first != NULL)
        { // comparing first group first elemment with all the elements of next group
          // and checking for one bit difference
          clone_second_copy = clone_second;
         while(clone_second !=NULL)
         {   
             bit_diff = check_one_bit(clone_first->var_minterm.bin_term, clone_second->var_minterm.bin_term);

             if(bit_diff == 1) 
                { if(first_set == 0)
                      { node1 = (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                        node1->var_updated = turn_bit_off(clone_first->var_minterm, clone_second->var_minterm);
                        clone_first->var_minterm.choosen = 1;//choosen for next itr
                        clone_second->var_minterm.choosen = 1;
                        struct updated_implicants_list *link;
                        link = NULL;
                        all_itr->all_headers[i] = (int *) node1;
                        node1->link = NULL;
                        first_set = 1; //first node is set
                      }
                  else
                      { // printf("%d %d %d\n", clone_first->var_minterm.dec_term, clone_second->var_minterm.dec_term, bit_diff );
                        node2= (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                        node2->var_updated = turn_bit_off(clone_first->var_minterm, clone_second->var_minterm);
                        clone_first->var_minterm.choosen = 1;//choosen for next itr
                        clone_second->var_minterm.choosen = 1;
                        node1->link = node2;
                        node2->link = NULL;
                        node1 = node2; 
                      }
                } // if bit_diff == 1 ends
             clone_second = clone_second -> link; 
          } //clone_second loop ends
          //sending less fortunate ones to next stage
        
          if(clone_first->var_minterm.choosen != 1)
          {   //printf("%d\n", clone_first->var_minterm.dec_term );
              if(first_set_last == 0)
                      { node_1 = (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                        
                        node_1->var_updated.dec_minterms = malloc(sizeof(int));
                        node_1->var_updated.bin_minterms = calloc(10, sizeof(int));
                        node_1->var_updated.dec_minterms[0] = clone_first->var_minterm.dec_term;
                        node_1->var_updated.num_dec = 1;
                        node_1->var_updated.bin_minterms = clone_first->var_minterm.bin_term;
                        struct updated_implicants_list *link;
                        link = NULL;
                        all_itr->all_headers[0] = (int *) node_1;
                        node_1->link = NULL;
                        first_set_last = 1; //first node is set
                      }
                  else
                      {  
                        node_2= (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                        node_2->var_updated.dec_minterms = malloc(sizeof(int));
                        node_2->var_updated.num_dec = 1;
                        node_2->var_updated.bin_minterms = calloc(10, sizeof(int));
                        node_2->var_updated.dec_minterms[0] = clone_first->var_minterm.dec_term;
                        node_2->var_updated.bin_minterms = clone_first->var_minterm.bin_term;
                        node_1->link = node_2;
                        node_2->link = NULL;
                        node_1 = node_2; 
                      }
          }
        
         clone_second = clone_second_copy;
         clone_first = clone_first->link;
         match_found = 0; //picking next number in first group so reset its match
        } //clone_first loop ends
        first_set = 0; //check next two groups, set node again
      }//for loop ends  


     return all_itr;

}
/******************************************************************************
 fucntion to check if two updated_implicants has same bits turned off and whether
 they differ by one bit. Takes input of two array of binary values of implicants
 diff by one and returns 0 oe 1 for condtion being false or true
****************************************************************************/
 int general_check_bit(int *array1, int *array2){
     int i, flag = 1;
     int bit_diff_count =0;
     for(i = 0; i<10; i++) 
     {   
         if(array1[i] !=array2[i]) bit_diff_count++;
         if( (array1[i] !=2 && array2[i] == 2) || (array1[i] ==2 && array2[i] != 2) ) flag = 0;
                  
     }
     if(bit_diff_count != 1) flag = 0; 
     return flag;

 }

/***************************************************************************
 function to update var_updated minterms pair which has same number of bits 
 turned off and rest bits differ by one. It takes input of two var-updated
 and returns new var_updated with appropriate bits turned off
 ****************************************************************************/
struct updated_implicants general_bit_off(struct updated_implicants first_min, struct updated_implicants second_min){
      int i,j;
      struct updated_implicants var;
      var.dec_minterms = malloc(2*sizeof(first_min.num_dec)*sizeof(int));
      var.bin_minterms = calloc(10, sizeof(int));
      for(i = 0; i<first_min.num_dec; i++) var.dec_minterms[i] = first_min.dec_minterms[i];
      for(i = first_min.num_dec, j = 0; i< (2*first_min.num_dec) && j < second_min.num_dec; i++, j++) 
        var.dec_minterms[i] = second_min.dec_minterms[j];
      var.num_dec = 2*first_min.num_dec;
      for(i=0; i<10; i++) 
       {   
         if(first_min.bin_minterms[i]==second_min.bin_minterms[i]) var.bin_minterms[i] = first_min.bin_minterms[i]; //set the bit as it
         else var.bin_minterms[i] = 2; //equivalent to - in the actual method
       }
      return var;

}

/***************************************************************************
 general function to update the list after first itr. takes input of the header
 of list formed after one iteration and link of header zeroth header, returns 
 header of updated list
 ****************************************************************************/
struct group_array *general_update(struct group_array *after_first, int *last_link){
     //may the force be with me?
     struct updated_implicants_list *clone_first, *clone_second, *clone_second_copy, *last_check;
     struct updated_implicants_list *node1, *node2, *node_1, *node_2;
     struct group_array *all_itr; 
     int bit_diff = 0;
     int first_set = 0;
     int first_set_last = 0; //used to set 0 node of all group list
     int i,j; //just an iterator! you must know by now
     int match_found = 0; //not matched with any one for one bit diff
     all_itr = (struct group_array *) malloc(sizeof(struct group_array));
     //setting zeroth node
     if(last_link != NULL)
     {
      all_itr->all_headers[0] = last_link;
      node_1 = (struct updated_implicants_list *) last_link;
      while(node_1->link !=NULL) node_1 = node_1 -> link;
      first_set_last = 1;
     }
     
     //setting rest of nodes
     for(i=1;i<10;i++)
     {
       struct updated_implicants_list *first_header = (struct updated_implicants_list *)after_first->all_headers[i];
       struct updated_implicants_list *second_header = (struct updated_implicants_list *)after_first->all_headers[i+1];
     
       clone_first = first_header;
       clone_second = second_header;
       while(clone_first != NULL)
        { // comparing first group first elemment with all the elements of next group
          // and checking for one bit difference
          clone_second_copy = clone_second;
         while(clone_second !=NULL)
         { 
             bit_diff = general_check_bit(clone_first->var_updated.bin_minterms, clone_second->var_updated.bin_minterms);
             if(bit_diff == 1) 
                { if(first_set == 0)
                      { node1 = (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                        node1->var_updated = general_bit_off(clone_first->var_updated, clone_second->var_updated);
                        clone_first->var_updated.choosen = 1;//choosen for next itr
                        clone_second->var_updated.choosen = 1;
                        struct updated_implicants_list *link;
                        link = NULL;
                        all_itr->all_headers[i] = (int *) node1;
                        node1->link = NULL;
                        first_set = 1; //first node is set
                      }
                  else
                      {  
                        node2= (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                        node2->var_updated = general_bit_off(clone_first->var_updated, clone_second->var_updated);
                        clone_first->var_updated.choosen = 1;//choosen for next itr
                        clone_second->var_updated.choosen = 1;
                        node1->link = node2;
                        node2->link = NULL;
                        node1 = node2; 
                      }
                } // if bit_diff == 1 ends
             clone_second = clone_second -> link; 
          } //clone_second loop ends
          //sending less fortunate ones to next stage
          
          if(clone_first->var_updated.choosen != 1)
          {   if(first_set_last == 0)
                 { 
                   node_1 = (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                   node_1->var_updated.dec_minterms = malloc((clone_first->var_updated.num_dec)*sizeof(int));
                   node_1->var_updated.num_dec = clone_first->var_updated.num_dec;
                   node_1->var_updated.bin_minterms = calloc(10, sizeof(int));
                   //setting dec term
                   for(j=0; j< (clone_first->var_updated.num_dec);j++) 
                    node_1->var_updated.dec_minterms[j] = clone_first->var_updated.dec_minterms[j];              
                   node_1->var_updated.bin_minterms = clone_first->var_updated.bin_minterms;
                   struct updated_implicants_list *link;
                   link = NULL;
                   all_itr->all_headers[0] = (int *) node_1;
                   node_1->link = NULL;
                   first_set_last = 1; //first node is set
                  
                 }    
              else
              {

                  node_2= (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                  node_2->var_updated.dec_minterms = malloc((clone_first->var_updated.num_dec)*sizeof(int));
                  node_2->var_updated.num_dec = clone_first->var_updated.num_dec;
                  node_2->var_updated.bin_minterms = calloc(10, sizeof(int));
                  //setting dec term
                  for(j=0; j< (clone_first->var_updated.num_dec);j++) 
                    node_2->var_updated.dec_minterms[j] = clone_first->var_updated.dec_minterms[j];              
                  node_2->var_updated.bin_minterms = clone_first->var_updated.bin_minterms;
                  node_1->link = node_2;
                  node_2->link = NULL;
                  node_1 = node_2; 
              }
                    
          }
        
         clone_second = clone_second_copy;
         clone_first = clone_first->link;
         match_found = 0; //picking next number in first group so reset its match
        } //clone_first loop ends
        first_set = 0; //check next two groups, set node again
      }//for loop ends  
     return all_itr;

}
/********************************************************************************
 function to complete 10 ieration required for completing iteration. takes input of
 header obtained after first itr and returns array of headers of final updated list
*********************************************************************************/
struct group_array *general_itr( struct group_array *after_first, int *last_link){
  int i,j,flag = 0;
  struct group_array *var, *var2;
  struct updated_implicants_list *duplicate, *duplicate_next = NULL; //used to remove duplicate implicants group
  var = general_update(after_first, last_link);
  for(j=3;j<12;j++) var = general_update(var, var->all_headers[0]); //makes sure all the terms are in all_headers[0]
  for(j=0;j<10;j++)
  { 
    duplicate = (struct updated_implicants_list *) var->all_headers[0];
    if(duplicate != NULL) duplicate_next = duplicate -> link;
    while(duplicate_next != NULL )
    { 
      for(i=0; i<10; i++)
        {  
           if(duplicate->var_updated.bin_minterms[i] != duplicate_next->var_updated.bin_minterms[i]) flag = 1;
           
        }
        if(!(flag))
          //delete the duplicate
        { 
          duplicate->link = duplicate_next -> link;
        }
       flag = 0;//checking next node, reset flag
       duplicate = duplicate->link;
       if(duplicate != NULL) duplicate_next = duplicate ->link;
       else duplicate_next = NULL;
    }
  } 
  return var;

}
/******************************************************************************
it takes input of array of minterms, final array of updaed minterms
,total noumbe rof groups formed in the final update state and total number of input 
variables! phew! that was a lot. reutrns a 1-d implementation of 2d array. help yourself
********************************************************************************/
int *lets_table(int *minterm_array, struct group_array *final, int total_groups, int num_min){
     struct updated_implicants_list *zeroth; //all the the implicants are only in zeroth head
     int row, col, i,j, total_dec, set;
     int next = 1;
     int k = 0;
     int *a = calloc( num_min, (total_groups*sizeof(int)));
     zeroth = (struct updated_implicants_list *) final->all_headers[0];
     //formation of table
      while(zeroth!=NULL)
      {
      for(i = 0; (i< zeroth->var_updated.num_dec);i++)
      {
         for(k = (next -1) * num_min, j = 0; k< next*num_min && j < num_min; k++, j++)
         { 
           if(minterm_array[j] == zeroth->var_updated.dec_minterms[i]) a[k] = 1;  
         }
        
      } //end of for i
      next ++;
      zeroth = zeroth -> link;
     }//end of while loop
     return a;

}

/********************************************************************************
function to update table. it takes input of array of a table with crosses set,
array of minterms, list of all the pairs formed after all the iterations, total
number of the pairs formed after all the iterations and total number of the minterms
we had. checks all the condtions, returns the pairs which are selected in this itr
and updates the table for next itr
*********************************************************************************/
struct group_array *find_essential(int *table, int *min_array, struct group_array *final, int total_groups, int num_min){
     struct updated_implicants_list *zeroth; //all the the implicants are only in zeroth head
     struct updated_implicants_list *node_ans1, *node_ans2; //what we return
     struct group_array *all_itr; //return still a group array! oh humanity :'(
     int first_set = 0;
     int row, col,set, row_off, row_off_copy, row_off_itr,col_off, col_off_itr, found_one = 0, i = 0, m = 0, j;
     int itr, row_counter = 0; //for accessing all the elements in zeroth
     int col_one; //for setting all the row of col one zero
     int row_dummy, col_dummy;
     int *off_rows = calloc(total_groups, sizeof(int));
     int *off_cols = calloc(num_min, sizeof(int)); 
     all_itr = (struct group_array *) malloc(sizeof(struct group_array));
     zeroth = (struct updated_implicants_list *)final->all_headers[0];
        for(col = 0; col < num_min; col ++) 
          {for(row = 0; row < total_groups;row++)
             {
              if(table[(row*num_min)+col] == 1) 
                {found_one++ ;
                 set = (row*num_min)+col;
                }
              } //row ends
              if(found_one == 1)
             { 
               off_rows[i] = (set-col)/num_min;
               i++; //used later: go only to ith element of the array
               row_off = (set-col)/num_min;
               off_cols[m] = col;
               while(zeroth != NULL)
               {
                  if(row_counter == row_off)
                  {
                    //formation of new updated_list to retrun
                    if(first_set == 0)
                    { node_ans1 = (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                      node_ans1->var_updated = zeroth->var_updated;
                      node_ans1->link = NULL;
                      all_itr->all_headers[0] = (int *) node_ans1;
                      first_set = 1;
                    }
                    else
                    {
                      node_ans2 = (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
                      node_ans2->var_updated = zeroth->var_updated;
                      node_ans1->link = node_ans2;
                      node_ans2->link = NULL;
                      node_ans1 = node_ans2; 

                    }
                  }//if row_counter ends
                  row_counter++;
                  zeroth = zeroth->link;
               }//while zeroth ends
               zeroth = (struct updated_implicants_list *)final->all_headers[0]; //reset for next loop
               row_counter = 0; //reset for next loop
               m++; //used later: go only to ith element of the array
               col_off = col; //??? too afraid to delete it and break the code

              }//if found_one ends
              found_one = 0;
        
          }//for col
          //turn off row
          for(j=0; j<i; j++) //get elements of essesntial row
          {  for(col_one = 0; col_one < num_min; col_one++)
            { if( table[(off_rows[j] * num_min) +col_one] == 1)
              { for(row_off_itr = 0; row_off_itr < total_groups; row_off_itr++) table[(row_off_itr*num_min) +col_one] = 0;
              }
           }
          //  printf("\n");
          //  for(row_dummy = 0; row_dummy < total_groups;row_dummy++)
          //    {
          //       for(col_dummy= 0; col_dummy < num_min; col_dummy++) //printf("%d,%d ",  (row_dummy*14)+col_dummy, c[(row_dummy*14)+col_dummy] );
          //       printf("%d ", table[(row_dummy*num_min)+col_dummy] );
          //       printf("\n");
             
          //    }
          //    printf("\n");
           }
          //turn off col
          for(j = 0; j< m; j++)
          {
           for(col_off_itr = off_cols[j]; col_off_itr < off_cols[j]+((total_groups)*num_min); col_off_itr += num_min)
              table[col_off_itr] = 0;
          }
        return all_itr;
}

/**************************************************************************************
function to check dominating column and turn off corresponding positions off in the
table, takes ipput of table, number of minters and total pairs formed 
*************************************************************************************/

void *check_dominating_col(int *table, int total_groups, int num_min){
     int col, row, col_all,row_dummy, col_dummy, one_match = 0;
     int not_dom = 0; 
     // printf("\n" );
     // for(row = 0; row < total_groups;row++)
     // {
     //    for(col = 0; col < num_min; col ++) printf("%d ", table[(row*num_min)+col]);
     //    printf("\n");
     // } 
     for(col = 0; col < num_min ; col ++) 
       {//checking each col with rest of cols each row at a time
        for(col_all = 0; col_all < num_min; col_all++ )
          { if(col_all != col) //skip the self col
            {
            for(row = 0; row < total_groups;row++)
              {
               if(table[(row*num_min)+col] == 0 && table[(row*num_min)+col_all] == 1) not_dom = 1; 
               if(table[(row*num_min)+col] == 1 && table[(row*num_min)+col_all] == 1) one_match = 1;
              }//end of for row
            } //end of if
             if(!(one_match)) not_dom = 1; //not even one col of other was 1 
             if(!(not_dom)) //is dominating
             {
                for(row = 0; row < total_groups;row++) table[(row*num_min)+col] = 0 ;
                if(col < (num_min -1)) col ++; //go to next col
                col_all = -1;
             }//end of update if
             //rest flags
             not_dom = 0;
             one_match = 0;
          }//end of col_all
        }//end of for col
}
/*************************************************************************************
function to check dominating row and turn of required positons in the table
**************************************************************************************/
void *check_dominating_row(int *table, int total_groups, int num_min){
    int col, row, row_all,row_dummy, col_dummy, one_match = 0;
     int not_dom = 0; 
     

     // for(row = 0; row < total_groups;row++)
     // {
     //    for(col = 0; col < num_min; col ++) printf("%d ", table[(row*num_min)+col]);
     //    printf("\n");
     // } 
     for(row = 0; row < total_groups ; row ++) 
       {//checking each col with rest of cols each row at a time
        for(row_all = 0; row_all < total_groups; row_all++ )
          { if(row_all != row) //skip the self col
            {
            for(col = 0; col < num_min;col++)
              {
               if(table[(row*num_min)+col] == 0 && table[(row_all*num_min)+col] == 1) not_dom = 1; 
               if(table[(row*num_min)+col] == 1 && table[(row_all*num_min)+col] == 1) one_match = 1;
              }//end of for col
            } //end of if
             if(!(one_match)) not_dom = 1; //not even one col of other was 1 
             if(!(not_dom)) //is dominating
             {
                for(col = 0; col < num_min;col++) table[(row_all*num_min)+col] = 0 ;
                row_all = -1;
             }//end of update if
             //rest flags
             not_dom = 0;
             one_match = 0;
          }//end of row_all
        }//end of for row
}
/***************************************************************************************
fucntion to check if final state has reached. takes input of table anc calculates to see
if sum of all the positons is zero and returns 0 (sum is not zero) and 1 if final state 
has reached
***************************************************************************************/
int check_sum(int *table, int total_groups, int num_min){
     int row_dummy,col_dummy, sum = 0;
     for(row_dummy = 0; row_dummy <total_groups;row_dummy++)
     {
        for(col_dummy= 0; col_dummy < num_min; col_dummy++)
        sum = sum + table[(row_dummy*num_min)+col_dummy];      
     }
     if (sum == 0) return 1;
     else return 0;
}
/**************************************************************************************
function to itteratively perform essenstial check, dominating column and row check until
all of the positons in the table are not off. min_array and group_array of final_final
are jsut needed to be passed to in check essentials
*****************************************************************************************/


struct group_array *table_itr(int *table, int *min_array, struct group_array *final_final, int total_groups, int num_min){
     int check,row, col,itr, k,first_set = 0, no_effect = 1;
     struct group_array *after_table, *preserve;
     struct updated_implicants_list *node1, *node2, *node_itr;
     preserve = (struct group_array *)malloc(sizeof(struct group_array));
    
     while(!(check_sum(table, total_groups, num_min)) && no_effect) //breaks when no more bits of the table are on
     {   no_effect = 0;
         after_table = find_essential(table, min_array, final_final, total_groups, num_min);
          // for(row = 0; row < total_groups;row++)
          //    {
          //       for(col = 0; col < num_min; col ++) printf("%d ", table[(row*num_min)+col] );
          //       printf("\n");
          //    } 


         node_itr = (struct updated_implicants_list *) after_table->all_headers[0];
         while(node_itr!=NULL)
         { no_effect = 1;  
          itr =node_itr->var_updated.num_dec;
             for(k=0; k<itr;k++) printf("%d ",node_itr->var_updated.dec_minterms[k]);
              printf("\n");
         if(!(first_set))
         {
             node1 = (struct updated_implicants_list *) malloc(sizeof(struct updated_implicants_list));
             preserve->all_headers[0] = (int *)node1;
             node1->var_updated = node_itr->var_updated;
             node1->link = NULL;
             first_set = 1;
         }
         else
         {
             node2 = (struct updated_implicants_list *)malloc(sizeof(struct updated_implicants_list));
             node2->var_updated = node_itr->var_updated;
             node1->link = node2;
             node2->link = NULL;
             node1 = node2;
         }//else ends
          node_itr = node_itr ->link;
        }//while ends
        printf("Generating prime implicants. Please wait... \n");
         check_dominating_col(table, total_groups, num_min);
         check_dominating_row(table, total_groups, num_min);
     }
     return preserve;
}





/*********************************************************************
 MAIN SECTION OF CODE
*********************************************************************/
main(){
  int i,j,l, itr,k,m,flag,n, counter =0;
  int row_dummy,col_dummy;
  /*test case*/
	//int a[] = {0, 1,2,3,4,5,6,7,8, 12};
  //int a[]={0,1,2,8,9,15,17,21,24,25,27,31};
  //int a[] = {1,5,6,7,11,12,13,15};
  //int a[] = {0,5,7,8,9,10,11,14,15};


  //int a[] = {0, 2, 5, 6, 7, 8, 10, 12, 13, 14, 15};
  //int a[] = {0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
  //int a[] = {0, 5, 6, 7, 9,10,13,14,15};
  //int a[] = {0, 2, 4, 5, 6, 7, 8, 10, 13, 15};
  //int a[] = {0,1,4,5,7,8,12,14,15,19,20,21,25,26,30,31};
  //int a[] = {0,1,3,5,7,9,10,16,17,18,19,20,22,23,27};
  //int a[] = {0,1,3,6,7,9,12,14,18,19,20,24,31};
  //int a[] = {0};
  //int a[] = {1,8,9,11,12,13,14,16,20,22,24,25,27,28,29,31,33,36,37,39,41,43,44,47,51,53,56,57,59,62,65,67,69,70,71,75,76,77,78,86,87,88,91,93,94,96,98,100,103,105,108,109,111,112,115,117,118,120,122,123,126};

  int row, col,num_min;
	struct minterm_list *var2, *var_2;
  struct group_array *group_idx;
  struct updated_implicants foo, boo, coo, doo;
  struct group_array *var, *var_itr2, *after_table;
  struct updated_implicants_list *lee, *lee2, *copy, *lets_see_table;
  int *b;

  //implementing random function
  
  srand(88);
  num_min = 600;
  int *a = malloc(num_min*sizeof(int));
  for(l = 0; l<num_min; l++)
  {
    a[l] = rand()%((int) pow(2.0, 10.0));
  printf("%d ", a[l]);
  }



	var2 = make_class_list(a, num_min);
  group_idx = make_group_array(var2);
  var = update_group_itr1(group_idx);

  var_itr2 = general_itr(var, var->all_headers[0]);
  for(j=0;j<10;j++)
  {
  lee = (struct updated_implicants_list *) var->all_headers[j];
  
  while(lee !=NULL)
  {
  itr =lee->var_updated.num_dec;
  for(k=0; k<itr;k++) printf("%d ",lee->var_updated.dec_minterms[k]);
  for(i=0; i<10; i++)
  {
   printf("%d", lee->var_updated.bin_minterms[i]);
  }
  printf("\n");
  lee = lee -> link;
  } //while
  }
  //print first table
  lee2 = (struct updated_implicants_list *) var_itr2->all_headers[0];
  while(lee2!=NULL)
  {
    counter++;
    lee2 = lee2 -> link;
  }
    b =lets_table(a, var_itr2, counter, num_min);
   // for(row = 0; row < counter;row++)
   //   {
   //      for(col = 0; col < num_min; col ++) printf("%d ", b[(row*num_min)+col] );
   //      printf("\n");
   //   } 

    after_table = table_itr(b,a, var_itr2,counter,num_min);
    printf("Final list of selected minterms is: \n");
    lets_see_table = (struct updated_implicants_list *) after_table->all_headers[0];
    while(lets_see_table !=NULL)
      { 
      itr =lets_see_table->var_updated.num_dec;
      for(k=0; k<itr;k++) printf("%d ",lets_see_table->var_updated.dec_minterms[k]);
      for(i=0; i<10; i++)
      {
       printf("%d", lets_see_table->var_updated.bin_minterms[i]);
      }
      printf("\n");
      lets_see_table = lets_see_table -> link;
      }
      

    //print udated table
    // for(row = 0; row < counter;row++)
    //  {
    //     for(col = 0; col < num_min; col ++) printf("%d ", b[(row*num_min)+col] );
    //     printf("\n");
    //  } 
   

}